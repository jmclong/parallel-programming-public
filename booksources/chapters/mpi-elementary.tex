% -*- latex -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%% This text file is part of the source of 
%%%% `Parallel Programming in MPI and OpenMP'
%%%% by Victor Eijkhout, copyright 2012-2021
%%%%
%%%% mpi-elementary.tex : elementary datatypes
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\Level 0 {Elementary data types}
\label{sec:elementary}
\index{datatype!elementary|(}

MPI has a number of elementary data types, corresponding to the 
simple data types of programming languages.
The names are made to resemble the types of C and~Fortran, 
for instance \indexmpishow{MPI_FLOAT} and \indexmpishow{MPI_DOUBLE} in~C,
versus
\indexmpishow{MPI_REAL} and \indexmpishow{MPI_DOUBLE_PRECISION} in~Fortran.

\begin{comment}
  MPI calls accept arrays of elements:
  \begin{lstlisting}
    double x[20];
    MPI_Send( x,20,MPI_DOUBLE, ..... )
  \end{lstlisting}
  so for a single element you need to take its address:
  \begin{lstlisting}
    double x;
    MPI_Send( &x,1,MPI_DOUBLE, ..... )
  \end{lstlisting}
\end{comment}

\Level 1 {C/C++}
\index{datatype!elementary!in C}

Here we illustrate the correspondence between a type used to declare a variable,
and how this type appears in MPI communication routines:
\begin{lstlisting}
long int i;
MPI_Send(&i,1,MPI_LONG_INT,target,tag,comm);
\end{lstlisting}

\begin{tabular}{|ll|}
  \hline
  C type&MPI type\\
  \hline
% char
\lstinline+char+&\indexmpidef{MPI_CHAR}\\
\lstinline+unsigned char+&\indexmpidef{MPI_UNSIGNED_CHAR}\\
\lstinline+char+&\indexmpidef{MPI_SIGNED_CHAR}\\
% int
\lstinline+short+&\indexmpidef{MPI_SHORT}\\
\lstinline+unsigned short+&\indexmpidef{MPI_UNSIGNED_SHORT}\\
\lstinline+int+&\indexmpidef{MPI_INT}\\
\lstinline+unsigned int+&\indexmpidef{MPI_UNSIGNED}\\
\lstinline+long int+&\indexmpidef{MPI_LONG}\\
\lstinline+unsigned long int+&\indexmpidef{MPI_UNSIGNED_LONG}\\
\lstinline+long long int+&\indexmpidef{MPI_LONG_LONG_INT}\\
% real
\lstinline+float+&\indexmpidef{MPI_FLOAT}\\
\lstinline+double+&\indexmpidef{MPI_DOUBLE}\\
\lstinline+long double+&\indexmpidef{MPI_LONG_DOUBLE}\\
% other
\lstinline+unsigned char+&\indexmpidef{MPI_BYTE}\\
(does not correspond to a C type)&\indexmpishow{MPI_PACKED}\\
\lstinline+MPI_Aint+&\indexmpishow{MPI_AINT}\\
  \hline
\end{tabular}

There is some, but not complete, support for \indexterm{C99} types.

See section~\ref{sec:mpi-byte-type} for \indexmpishow{MPI_Aint}
and more about byte counting.

\Level 1 {Fortran}
\index{datatype!elementary!in Fortran}
\lstset{style=reviewcode,language=Fortran} %pyskip

\begin{tabular}{|ll|}
  \hline
\indexmpidef{MPI_CHARACTER}&Character(Len=1)\\
% int
\indexmpidef{MPI_INTEGER}&\\
\indexmpidef{MPI_INTEGER1}&\\
\indexmpidef{MPI_INTEGER2}&\\
\indexmpidef{MPI_INTEGER4}&\\
\indexmpidef{MPI_INTEGER8}&(common compiler extension; not standard)\\
\indexmpidef{MPI_INTEGER16}&\\
% real
\indexmpidef{MPI_REAL}&\\
\indexmpidef{MPI_DOUBLE_PRECISION}&\\
\indexmpidef{MPI_REAL2}&\\
\indexmpidef{MPI_REAL4}&\\
\indexmpidef{MPI_REAL8}&\\
% complex
\indexmpidef{MPI_COMPLEX}&\\
\indexmpidef{MPI_DOUBLE_COMPLEX}&Complex(Kind=Kind(0.d0))\\
% other
\indexmpidef{MPI_LOGICAL}&\\
\indexmpidef{MPI_PACKED}&\\
  \hline
\end{tabular}

Not all these types need be supported, for instance
\indexmpishow{MPI_INTEGER16} may not exist, in which case it will be
equivalent to \indexmpishow{MPI_DATATYPE_NULL}.

The default integer type \indexmpishow{MPI_INTEGER} is equivalent to
\lstinline{INTEGER(KIND=MPI_INTEGER_KIND)}.

\Level 2 {Big data types}

The C type \indexmpishow{MPI_Count} corresponds to an integer of type
\indexmpidef{MPI_COUNT_KIND}, used most prominently in `big data'
routines such as \indexmpishow{MPI_Type_size_x} 
(section~\ref{sec:mpi-type-size}):
\begin{lstlisting}
Integer(kind=MPI_COUNT_KIND) :: count
call MPI_Type_size_x(my_type, count)
\end{lstlisting}

\begin{mpifour}
  For every routine \lstinline{MPI_Something}
  with an \lstinline{int} count parameter,
  there is a corresponding routine \lstinline{MPI_Something_c}
  with an \lstinline{MPI_Count} parameter.
\end{mpifour}

\Level 2 {Byte counting types}
\label{sec:mpi-byte-count}

Kind \indexmpishow{MPI_ADDRESS_KIND} is used for \indexmpishow{MPI_Aint}
quantities, used in \ac{RMA} windows; see section~\ref{sec:mpi-put}.

The \indexmpidef{MPI_OFFSET_KIND}
is used to define \indexmpishow{MPI_Offset} quantities,
used in file I/O; section~\ref{sec:mpi-filepoint}.

\Level 2 {Fortran90 kind-defined types}
\label{sec:f90-types}

If your Fortran code uses \lstinline{KIND} to define scalar types with
specified precision, these do not in general correspond to any
predefined MPI datatypes. Hence the following routines exist to make
\emph{MPI equivalences of Fortran scalar types}%
\index{Fortran!MPI equivalences of scalar types}:
\indexmpiref{MPI_Type_create_f90_integer}
\indexmpiref{MPI_Type_create_f90_real}
\indexmpiref{MPI_Type_create_f90_complex}.

Examples:
\begin{lstlisting}
INTEGER ( KIND = SELECTED_INTEGER_KIND(15) ) , &
 DIMENSION(100) :: array INTEGER :: root , integertype , error 

CALL MPI_Type_create_f90_integer( 15 , integertype , error )
CALL MPI_Bcast ( array , 100 , &
 integertype , root , MPI_COMM_WORLD , error )

REAL ( KIND = SELECTED_REAL_KIND(15 ,300) ) , &
 DIMENSION(100) :: array
CALL MPI_Type_create_f90_real( 15 , 300 , realtype , error )

COMPLEX ( KIND = SELECTED_REAL_KIND(15 ,300) ) , &
 DIMENSION(100) :: array 
CALL MPI_Type_create_f90_complex( 15 , 300 , complextype , error )
\end{lstlisting}
\lstset{style=reviewcode,language=C} %pyskip

\Level 1 {Python}
\index{datatype!elementary!in Python}

In python, all buffer data comes from \indexterm{Numpy}.

\begin{tabular}{|ll|}
  \hline
  mpi4py type&NumPy type\\
  \hline
  \n{MPI.INT}&\n{np.intc}\\
  \n{MPI.LONG}&\n{np.int}\\
  \n{MPI.FLOAT}&\n{np.float32}\\
  \n{MPI.DOUBLE}&\n{np.float64}\\
  \hline
\end{tabular}

Note that numpy integers correspond to \n{long int}s in~C,
and are therefore 8~bytes long.
This is no problem if you send and receive contiguous buffers of integers,
but it may trip you up in cases where the actual size of the integer matters,
such as in derived types, or window definitions.

Examples:

\pverbatimsnippet[examples/mpi/p/inttype.py]{npintc}

\pverbatimsnippet{bufallocp}

\Level 1 {Byte addressing type}
\label{sec:mpi-byte-type}

So far we have mostly been taking about datatypes in the context of
sending them. The \indexmpidef{MPI_Aint} type is not so much for
sending, as it is for describing the size of objects, such as the size
of an \indexmpishow{MPI_Win} object; section~\ref{sec:windows}.

Addresses have type \indexmpishow{MPI_Aint} The start of the address range is
given in \indexmpishow{MPI_BOTTOM}.

Variables of type \indexmpishow{MPI_Aint} can be sent as \indexmpidef{MPI_AINT}:
\begin{lstlisting}
MPI_Aint address;
MPI_Send( address,1,MPI_AINT, ... );
\end{lstlisting}
See section~\ref{sec:win-dynamic} for an example.

In order to prevent overflow errors in byte calculations
there are support routines \indexmpidef{MPI_Aint_add}
\begin{lstlisting}
MPI_Aint MPI_Aint_add(MPI_Aint base, MPI_Aint disp)
\end{lstlisting}
and similarly \indexmpidef{MPI_Aint_diff}.

See also the \indexmpishow{MPI_Sizeof}
(section~\ref{sec:mpi-type-match})
and
\indexmpishow{MPI_Get_address} routines.

\Level 2 {Fortran}

The equivalent of
%\indexmpishowsub{MPI_Aint}{in Fortran}
\indexmpishowf{MPI_Aint} in Fortran
is an integer of kind \indexmpidef{MPI_ADDRESS_KIND}:
\lstset{style=reviewcode,language=Fortran} %pyskip
\begin{lstlisting}
integer(kind=MPI_ADDRESS_KIND) :: winsize
\end{lstlisting}
\lstset{style=reviewcode,language=C} %pyskip

Fortran lacks a \n{sizeof} operator to query the sizes of datatypes.
Since sometimes exact byte counts are necessary,
for instance in one-sided communication,
Fortran can use the (deprecated) \indexmpiref{MPI_Sizeof} routine.
See section~\ref{sec:mpi-type-match} for details.

Example usage in \indexmpishow{MPI_Win_create}:
\lstset{language=Fortran} %pyskip
\begin{lstlisting}
call MPI_Sizeof(windowdata,window_element_size,ierr)
window_size = window_element_size*500
call MPI_Win_create( windowdata,window_size,window_element_size,... )
\end{lstlisting}
\lstset{language=C} %pyskip

\Level 2 {Python}

Here is a good way for finding the size of \indexterm{numpy} datatypes
in bytes:
\pverbatimsnippet{windispp}% source is in one-sided chapter

\Level 1 {Matching language type to MPI type}
\label{sec:mpi-type-match}

In some circumstances you may want to find the MPI type
that corresponds to a type in your programming language.
\begin{itemize}
\item In C++ functions and classes can be templated,
  meaning that the type is not fully known:
\begin{lstlisting}
template<typename T> {
class something<T> {
public:
  void dosend(T input) {
    MPI_Send( &input,1,/* ????? */ );
  };
};
\end{lstlisting}
(Note that in \ac{MPL} this is hardly ever needed
because MPI calls are templated there.)
\item Petsc installations use a generic identifier \indexpetscshow{PetscScalar}
  (or \indexpetscshow{PetscReal})
  with a configuration-dependent realization.
\item The size of a datatype is not always statically known, for instance if
the Fortran \indextermtt{KIND} keyword is used.
\end{itemize}

Here are some MPI mechanisms that address this problem.

\Level 2 {Type matching in C}
\label{sec:mpi-type-match-c}

Datatypes in~C can be translated to MPI types with
%
\indexmpiref{MPI_Type_match_size}
%
where the \lstinline{typeclass} argument is one of
\indexmpishow{MPI_TYPECLASS_REAL},
\indexmpishow{MPI_TYPECLASS_INTEGER},
\indexmpishow{MPI_TYPECLASS_COMPLEX}.

\cverbatimsnippet[examples/mpi/c/typematch.c]{typematchc}

The space that MPI takes for a structure type can be queried in a
variety of ways. First of all \indexmpiref{MPI_Type_size} counts the
\emph{datatype size}\index{MPI!datatype!size} as the 
number of bytes occupied by the data in a type. That means that in an
\emph{MPI vector datatype}\index{MPI!datatype!vector} it does not
count the gaps.
%
\cverbatimsnippet[examples/mpi/c/typesize.c]{vectortypesize}

\begin{comment}
In C, the \indexmpidef{MPI_Datatype} type is defined through the pre-processor,
allowing you to write:
\cverbatimsnippet{datatypevar}
\end{comment}

\Level 2 {Type matching in Fortran}
\label{sec:mpi-type-match-f}

In Fortran, the size of the datatype in the language can be obtained with
\indexmpishow{MPI_Sizeof} (note the nonoptional error parameter!).
This routine is deprecated in \mpistandard{4}: use of
\indextermtt{storage_size} and/or \indextermtt{c_sizeof} is recommended.

\fverbatimsnippet[examples/mpi/c/typematch.c]{typematchf}

Petsc has its own translation mechanism; see section~\ref{sec:petsc-scalar}.

\index{datatype!elementary|)}

